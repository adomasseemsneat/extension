# extension

bringing all your favourite notion-enhancer features to the browser (wip)

> considering that using notion in the browser is more lightweight & probably preferred for some people since it means they have less apps open, i've always planned to eventually port the enhancer to be a chrome extension.
>
> since the enhancer focuses on being able to manipulate notion's app files, it's not going to be a very friendly transfer to web - a few features will have to be lost (e.g. frameless mode & tabs, of course) and the extensions system will need to be completely rebuilt.
>
> to make this work better, i'm splitting things up: an mod repository, a chrome extension, and an app loader.
>
> i'll be building the enhancer chrome-first from now on, since it'll be more limited there (and it's easier to add extra features when porting than to take features out), and then releasing the app loader a little afterwards. both the app loader and the chrome extension will source the same themes & extensions from the mod repository, and updates & releases of individual mods won't require waiting for the enhancer version anymore.

-- from the #announcements channel of the [notion-enhancer discord](https://discord.gg/sFWPXtA)
